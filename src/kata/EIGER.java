package kata;

import java.util.HashMap;

/**
 * Simulates the simple EIGER programming language
 */
public class EIGER {
    private static HashMap<String, Integer> variables;

    public EIGER() {
        variables = new HashMap<>();
    }

    public boolean define(int integer, String variableName) {
        if (!isValidInput(integer, variableName)) {
            return false;
        }
        variables.put(variableName, integer);
        return true; // it was defined.
    }

    public String eval(String variableOne, Character operator, String variableTwo) {
        boolean variableOneExists = variables.containsKey(variableOne);
        boolean variableTwoExists = variables.containsKey(variableTwo);
        if (!variableOneExists || !variableTwoExists) {
            return "undefined";
        }
        if (!isValidOperator(operator)) {
            throw new IllegalArgumentException("Invalid operator");
        }
        int var1 = variables.get(variableOne);
        int var2 = variables.get(variableTwo);
        return switch (operator) {
            case '<' -> "result: " + (var1 < var2);
            case '=' -> "result: " + (var1 == var2);
            case '>' -> "result: " + (var1 > var2);
            default -> "error. Should not have reached this point";
        };
    }

    private boolean isValidOperator(Character operator) {
        return switch (operator) {
            case '<', '=', '>' -> true;
            default -> false;
        };
    }

    private boolean isValidInput(int integer, String variableName) {
        if (variableName.length() > 20) return false;
        if (integer > 10000) return false;
        if (integer < -10000) return false;
        return true;
    }
}
