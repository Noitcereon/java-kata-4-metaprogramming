package kata;

public class Main {

    public static void main(String[] args) {
        // write your code here
        EIGER simulation = new EIGER();
        simulation.define(5, "five");
        simulation.define(6, "six");
        printResult(simulation.eval("five", '<', "six"));
        printResult(simulation.eval("five", '>', "six"));
        printResult(simulation.eval("5", '>', "six"));
        printResult(simulation.eval("six", '=', "six"));
        printResult(simulation.eval("five", '=', "six"));

    }

    private static void printResult(String output) {
        System.out.println(output);
    }
}
